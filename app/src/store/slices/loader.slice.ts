import { createSlice } from '@reduxjs/toolkit';

const loaderSlice = createSlice({
    name: 'loader',
    initialState: false,
    reducers: {
      show: state => true,
      hide: state => false,
    }
})

const { actions, reducer } = loaderSlice;
const { show, hide } = actions;

export {
    reducer,
    show,
    hide,
}