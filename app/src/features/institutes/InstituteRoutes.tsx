import React, { FC } from 'react';
import { useRouteMatch, Switch, Route } from "react-router-dom";
import {AddInstitute} from "./AddInstitute";
import {Institutes} from "./Institutes";
import { UpdateInstitute } from './UpdateInstitute';

export const InstituteRoutes: FC = () => {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route path={`${match.path}/new`}>
                <AddInstitute />
            </Route>
            <Route path={`${match.path}/update/:id`}>
                <UpdateInstitute />
            </Route>
            <Route path={match.path}>
                <Institutes />
            </Route>
        </Switch>
    );
}