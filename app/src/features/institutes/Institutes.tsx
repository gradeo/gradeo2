import React, { FC, useState, useEffect } from 'react'
import { Button, Grid, Typography, Breadcrumbs, Link } from '@material-ui/core';
import { InstitutesTable } from './components/InstitutesTable';
import { InstituteListItemDto } from '../../shared/models/institutes/InstituteListItemDto';
import { getInstitutes } from '../../shared/api/institutes/getInstitutes';
import { show as showLoader, hide as hideLoader } from '../../store/slices/loader.slice';
import { useDispatch } from "react-redux";
import { toast } from 'react-toastify';
import { Link as RouterLink } from "react-router-dom";
import AddIcon from "@material-ui/icons/Add";
import { Panel } from '../../shared/components/Panel';
import InstitutesSearchField from './components/InstitutesSearchField';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     '& > * + *': {
//       marginTop: theme.spacing(2),
//     },
//   },
// }));

export const Institutes: FC = () => {
    const [items, setItems] = useState<InstituteListItemDto[]>([])
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(showLoader());
        getInstitutes()
            .then(institutes => setItems(institutes))
            .catch(() => toast.error('Не удалось загрузить данные'))
            .finally(() => dispatch(hideLoader()))
    }, [dispatch])

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Panel>
                        <Grid container item xs={12}>
                            <Grid item xs={8}>
                                <Typography variant="h6">
                                    Институты
                            </Typography>
                                <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
                                    <Link color="inherit" component={RouterLink} to="/">
                                        Главная
                                    </Link>
                                    <Typography color="textPrimary">Институты</Typography>
                                </Breadcrumbs>
                            </Grid>
                            <Grid
                                item
                                container
                                justify="flex-end"
                                alignItems="center"
                                xs={4}
                            >
                                <Grid item>
                                    <Button component={RouterLink}
                                        variant="contained"
                                        color="primary"
                                        disableElevation
                                        to="/institutes/new"
                                        startIcon={<AddIcon />}
                                    >
                                        Новый институт
                                </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Panel>
                </Grid>
                <Grid item xs={12}>
                    <InstitutesSearchField />
                </Grid>
                <Grid item xs={12}>
                    <InstitutesTable items={items} />
                </Grid>
            </Grid>
        </div>
    );
}