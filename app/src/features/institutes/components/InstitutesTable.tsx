import React, { FC } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Link
} from '@material-ui/core';
import { Link as RouterLink } from "react-router-dom";
import { InstituteListItemDto } from '../../../shared/models/institutes/InstituteListItemDto';
import { NotFoundCell } from '../../../shared/components/NotFoundCell';


interface InstitutesTableProps {
    items: InstituteListItemDto[];
}

export const InstitutesTable: FC<InstitutesTableProps> = ({ items }) => {
    let content: JSX.Element;

    if (items && items.length) {
        content = (
            <TableBody>
                {
                    items.map((item) => (
                        <TableRow key={item.name}>
                            <TableCell scope="row">
                                <Link 
                                    component={RouterLink}
                                    color="primary"
                                    to={`/institutes/update/${item.id}`}
                                >
                                    {item.id}
                                </Link>
                            </TableCell>
                            <TableCell>{item.name}</TableCell>
                            <TableCell align="right">{item.departmentsCount}</TableCell>
                        </TableRow>
                    ))
                }
            </TableBody>);
    }
    else {
        content = (
            <TableBody>
                <TableRow>
                    <NotFoundCell colSpan={3} />
                </TableRow>
            </TableBody>
        )
    }

    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>№</TableCell>
                        <TableCell>Название</TableCell>
                        <TableCell align="right">Кол-во кафедр</TableCell>
                    </TableRow>
                </TableHead>
                { content }
            </Table>
        </TableContainer>
    );
}