import React, { FC } from 'react'
import { Typography, Box, Grid } from '@material-ui/core';
import { Panel } from '../shared/components/Panel';
import CurrentTasksTable from './home/CurrentTasks';
import CurrentLessonsTable from './home/CurrentLessons';

export const Home: FC = () => {
    return (
        <div>
            <Typography variant="h5" gutterBottom>
                Добро пожаловать,
                <Box style={{ marginLeft: "12px" }} component="span" color="text.secondary">Денис Михайлович</Box>
            </Typography>
            <Box color="text.secondary">
                <Typography variant="subtitle1" gutterBottom>
                    Мы собрали данные для вас.
                </Typography>
            </Box>
            <Grid container spacing={2}>
                <Grid item xs={12} container spacing={2} justify="space-between">
                    <Grid item xs={3}>
                        <Panel>
                            <Typography variant="h3">
                                3
                        </Typography>
                            <Box color="text.secondary">
                                <Typography variant="subtitle1" gutterBottom>
                                    группы
                            </Typography>
                            </Box>
                        </Panel>
                    </Grid>
                    <Grid item xs={3}>
                        <Panel>
                            <Typography variant="h3">
                                2
                        </Typography>
                            <Box color="text.secondary">
                                <Typography variant="subtitle1" gutterBottom>
                                    предмета
                            </Typography>
                            </Box>
                        </Panel>
                    </Grid>
                    <Grid item xs={3}>
                        <Panel>
                            <Typography variant="h3">
                                15
                        </Typography>
                            <Box color="text.secondary">
                                <Typography variant="subtitle1" gutterBottom>
                                    заданий нужно проверить
                            </Typography>
                            </Box>
                        </Panel>
                    </Grid>
                    <Grid item xs={3}>
                        <Panel>

                            <Box color="warning.main">
                                <Typography variant="h3">
                                    12
                            </Typography>
                                <Typography variant="subtitle1" gutterBottom>
                                    проблемных студентов
                            </Typography>
                            </Box>
                        </Panel>
                    </Grid>
                </Grid>
                <Grid container item xs={12} spacing={2}>
                    <Grid item xs={6}>
                        <Typography variant="subtitle1">
                            Активность сдачи заданий
                        </Typography>
                        <CurrentTasksTable />
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="subtitle1">
                            Предстоящие занятия
                        </Typography>
                        <CurrentLessonsTable />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}