import React, { FC, useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Button,
  FormHelperText,
  InputLabel,
  FormControl,
  Input,
  MenuItem,
  Select,
} from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";
import { addDepartment } from "../../shared/api/departments/addDepartments";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { Panel } from "../../shared/components/Panel";
import {
  show as showLoader,
  hide as hideLoader,
} from "../../store/slices/loader.slice";
import { useDispatch } from "react-redux";
import { getInstitutes } from "../../shared/api/institutes/getInstitutes";
import { InstituteListItemDto } from "../../shared/models/institutes/InstituteListItemDto";

type AddDepartmentFormData = {
  name: string;
  instituteId: number;
};

export const AddDepartment: FC = () => {
  const [items, setItems] = useState<InstituteListItemDto[]>([]);
  const dispatch = useDispatch();
  let history = useHistory();

  useEffect(() => {
    dispatch(showLoader());
    getInstitutes()
      .then((institutes) => setItems(institutes))
      .catch(() => toast.error("Не удалось загрузить данные"))
      .finally(() => dispatch(hideLoader()));
  }, [dispatch]);

  const { register, handleSubmit, errors, control } = useForm<AddDepartmentFormData>();

  const onSubmit = handleSubmit(async ({ name, instituteId }) => {
    await addDepartment({ name, instituteId });

    history.push("/departments");

    toast.success("Данные успешно сохранены");
  });

  return (
    <form onSubmit={onSubmit}>
      <Panel>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h6" component="div">
              Новая кафедра
            </Typography>
          </Grid>
          <Grid container item xs={12} spacing={2}>
            <Grid item xs={6}>
              <FormControl style={{ width: "100%" }} error={!!errors.name}>
                <InputLabel htmlFor="name">Название</InputLabel>
                <Input
                  id="name"
                  name="name"
                  inputRef={register({ required: true })}
                />
                {errors.name && (
                  <FormHelperText id="name-error">
                    Название не может быть пустым
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>
            <Grid item xs={2}>
              <FormControl
                style={{ width: "100%" }}
                error={Boolean(errors.instituteId)}
              >
                <InputLabel id="demo-simple-select-required-label">Институт</InputLabel>
                <Controller
                  as={
                    <Select>
                      {items.map((item) => (
                        <MenuItem value={item.id}>{item.name}</MenuItem>
                      ))}
                    </Select>
                  }
                  control={control}
                  name="instituteId"
                  defaultValue=""
                  rules={{ required: "Выберите институт" }}
                />
                <FormHelperText>
                  {errors.instituteId && errors.instituteId.message}
                </FormHelperText>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container item xs={12}>
            <Button type="submit" color="primary">
              Добавить кафедру
            </Button>
          </Grid>
        </Grid>
      </Panel>
    </form>
  );
};
