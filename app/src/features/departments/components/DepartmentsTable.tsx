import { DepartmentListItemDto } from "../../../shared/models/departments/DepartmentListItemDto";
import React, { FC } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from '@material-ui/core';
import { NotFoundCell } from '../../../shared/components/NotFoundCell';

interface DepartmentsTableProps {
    items: DepartmentListItemDto[];
}

export const DepartmentsTable: FC<DepartmentsTableProps> = ({ items }) => {
    let content: JSX.Element;

    if (items && items.length) {
        content = (
            <TableBody>
                {items.map((item) => (
                    <TableRow key={item.name}>
                        <TableCell scope="row"> {item.id} </TableCell>
                        <TableCell>{item.name}</TableCell>
                        <TableCell align="right">{item.instituteName}</TableCell>
                    </TableRow>
                ))}
            </TableBody>);
    }
    else {
        content = (
            <TableBody>
                <TableRow>
                    <NotFoundCell colSpan={3} />
                </TableRow>
            </TableBody>
        )
    }

    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>№</TableCell>
                        <TableCell>Название</TableCell>
                        <TableCell>Институт</TableCell>
                    </TableRow>
                </TableHead>
                {content}
            </Table>
        </TableContainer>
    )
}
