import { StudentsListItemDto } from "../../../shared/models/StudentsListItemDto";
import React, { FC } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from '@material-ui/core';

 interface StudentsTableProps {
     items: StudentsListItemDto[];
 }
 
export const StudentsTable: FC<StudentsTableProps> = ({ items }) => {
    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Имя</TableCell>
                        <TableCell align="center">Группа</TableCell>
                        <TableCell align="right">Кафедра</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {items.map((item) => (
                        <TableRow key={item.name}>
                            <TableCell>{item.name}</TableCell>
                            <TableCell>{item.groupName}</TableCell>
                            <TableCell align="right">{item.departmentName}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}