import React, { FC } from "react";
import UnderDevelopmentImage from '../../assets/images/under-development.svg'
import { Grid, Typography, Box, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";

export const UnderDevelopment: FC = () => {
    return (
        <Grid container justify="center" spacing={1}>
            <Grid item>
                <img src={UnderDevelopmentImage} width="256" height="256" alt="Page not found" />
            </Grid>
            <Grid item xs={12} container justify="center">
                <Typography variant="h5">
                    Страница в разработке
                </Typography>
            </Grid>
            <Grid item xs={12} container justify="center">
                <Box color="text.secondary">
                    <Typography variant="subtitle1">
                        Эта страница не спит ночами и готовится к сдаче своего диплома
                    </Typography>
                </Box>
            </Grid>
            <Grid item xs={12} container justify="center">
                <Button 
                    color="primary" 
                    component={Link} 
                    to="/"
                    startIcon = {<ArrowBack />}
                >
                    Вернуться на главную
                </Button>
            </Grid>
        </Grid>
    )
}