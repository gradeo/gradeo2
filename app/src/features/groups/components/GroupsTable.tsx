import { GroupsListItemDto } from "../../../shared/models/GroupsListItemDto";
import React, { FC } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
} from '@material-ui/core';

interface GroupsTableProps {
    items: GroupsListItemDto[];
}

export const GroupsTable: FC<GroupsTableProps> = ({ items }) => {
    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>№</TableCell>
                        <TableCell>Название</TableCell>
                        <TableCell>Институт</TableCell>
                        <TableCell>Кафедра</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {items.map((item) => (
                        <TableRow key={item.name}>
                            <TableCell scope="row">
                                {item.id}
                            </TableCell>
                            <TableCell>{item.name}</TableCell>
                            <TableCell>{item.instituteName}</TableCell>
                            <TableCell>{item.departmentName}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
