import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
  },
});

function createData(subject: string, group: string, place: string, date: string) {
  return { subject, place, group, date };
}

const rows = [
  createData("П:ЯВУ", "ИВТ-20-1", "А-101", "05.09.2020 10:10"),
  createData("П:ЯВУ", "ИВТ-20-2", "А-101", "05.09.2020 11:50"),
  createData("Web", "ИВТ-18-1", "А-505", "06.09.2020 8:30"),
];

export default function CurrentLessonsTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Предмет</TableCell>
            <TableCell>Группа</TableCell>
            <TableCell>Аудитория</TableCell>
            <TableCell>Дата/время</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.date}>
              <TableCell component="th" scope="row">
                {row.subject}
              </TableCell>
              <TableCell>{row.group}</TableCell>
              <TableCell>{row.place}</TableCell>
              <TableCell>{row.date}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}