import { GroupsListItemDto } from "../models/GroupsListItemDto";
import { BASE_URL } from "./base-url";

export async function getGroups(): Promise<GroupsListItemDto[]> {
    const response = await fetch(`${BASE_URL}/api/groups`, {
        method: 'GET',
        mode: 'cors',
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem('authentication')}`,
            'Accept'       : 'application/json',
        }
    });
    const items = await response.json();
    return items;
}