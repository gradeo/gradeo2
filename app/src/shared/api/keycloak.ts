import Keycloak, { KeycloakInitOptions } from 'keycloak-js'

const keycloak = Keycloak({
    realm: process.env.NODE_ENV === "development" ? "Local" : "Gradeo",
    // TODO : use relative url to keycloak container
    url: "https://auth.gradeo.ru/auth/",
    clientId: "frontend"
});

export const keycloakProviderInitConfig: KeycloakInitOptions = {
  onLoad: 'login-required',
}

export default keycloak