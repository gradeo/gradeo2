import { InstituteListItemDto } from "../../models/institutes/InstituteListItemDto";
import { BASE_URL } from "../base-url";

export async function getInstitutes(): Promise<InstituteListItemDto[]> {
  const response = await fetch(`${BASE_URL}/api/institutes`, {
    method: "GET",
    mode: "cors",
    headers: {
      Authorization: `Bearer ${sessionStorage.getItem("authentication")}`,
      Accept: "application/json",
    },
  });
  const items = await response.json();
  return items;
}
