import React, { FC } from 'react';
import { usePaperStyling } from '../styling/usePaperStyling';
import { Paper } from '@material-ui/core';

export const Panel: FC = ({ children }) => {
    const classes = usePaperStyling();

    return (
        <Paper className={classes.paper}>
            {children}
        </Paper>
    )
}