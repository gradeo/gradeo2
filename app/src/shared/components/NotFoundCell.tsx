import React, { FC } from 'react';
import NoDataImage from '../../assets/images/empty.svg'
import { TableCell, Grid, Box, Typography } from '@material-ui/core';

interface NotFoundCellProps {
    colSpan: number;
}

export const NotFoundCell: FC<NotFoundCellProps> = ({ colSpan }) => {
    return (
        <TableCell colSpan={colSpan}>
            <Grid container justify="center" spacing={1}>
                <Grid item>
                    <img src={NoDataImage} width="128" height="128" alt="No data" />
                </Grid>
                <Grid item xs={12} container justify="center">
                    <Box color="text.secondary">
                        <Typography variant="subtitle1">
                            К сожалению, ничего не нашлось <span aria-label="cry" role="img">😓</span>
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </TableCell>
    )
}