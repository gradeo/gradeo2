export interface InstituteListItemDto {
    id: number;
    name: string;
    departmentsCount: number;
}