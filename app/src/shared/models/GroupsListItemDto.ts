export interface GroupsListItemDto {
    id: number;
    name: string;
    instituteId: number;
    departmentId: number;
    instituteName: string;
    departmentName: string;

}