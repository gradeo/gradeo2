import React, {FC} from 'react';
import {Collapse, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import useStyles from "../../layout-styles";
import {ExpandLess, ExpandMore} from "@material-ui/icons";

interface DrawerSectionProps {
    icon: JSX.Element,
    title: string,
}

export const DrawerSection: FC<DrawerSectionProps> = ({icon, title, children}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    return (
        <>
            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    {icon}
                </ListItemIcon>
                <ListItemText>
                    {title}
                </ListItemText>
                {open ? <ExpandLess/> : <ExpandMore/>}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding className={classes.nestedList}>
                    { children }
                </List>
            </Collapse>
        </>
    );
}