import React, { FC } from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { Link } from "react-router-dom";

interface DrawerLinkProps {
    icon?: JSX.Element,
    title: string,
    to: string
}

export const DrawerLink: FC<DrawerLinkProps> = ({ icon, title, to }) => {
    return <ListItem button component={Link} to={to}>
        { icon && <ListItemIcon>
            {icon}
        </ListItemIcon>
        }
        <ListItemText inset={!icon}>
            {title}
        </ListItemText>
    </ListItem>
}