import React, { FC } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            position: 'fixed',
            zIndex: 1200
        },
    }),
);

export const AppLoader: FC = () => {
    const classes = useStyles();
    const isLoaderVisible = useSelector((state: RootState) => state.loader);

    console.log(isLoaderVisible);

    if (!isLoaderVisible) {
        return null;
    }

    return (
        <div className={classes.root}>
            <LinearProgress color="primary" />
        </div>
    );
}