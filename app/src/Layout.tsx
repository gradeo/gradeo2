import React, { FC } from "react";
import clsx from "clsx";

import CssBaseline from "@material-ui/core/CssBaseline";
import useStyles from "./layout-styles";

import { Switch, Route, Redirect } from "react-router-dom";
import { Home } from "./features/Home";
import { AppNavBar } from "./layout/navbar/AppNavBar";
import { AppLoader } from "./layout/loader/AppLoader";
import { AppDrawer } from "./layout/drawer/Drawer";
import { Departments } from "./features/departments/Departments";
import { Groups } from "./features/groups/Groups";
import { RootState } from "./store/store";
import { useSelector } from "react-redux";
import { Import } from "./features/Import";
import { Students } from "./features/students/Students";
import { Login } from "./features/Login";
import { InstituteRoutes } from "./features/institutes/InstituteRoutes";
import { NotFound } from "./features/not-found/NotFound";
import { UnderDevelopment } from "./features/under-development/UnderDevelopment";
import { useKeycloak } from "@react-keycloak/web";

const Layout: FC = () => {
  const [, initialized] = useKeycloak();

  const classes = useStyles();
  const drawerOpen = useSelector((state: RootState) => state.drawer);

  if (!initialized) {
    return <div>Перенаправление на страницу авторизации...</div>;
  }

  return (
    <div>
      <AppLoader />
      <div className={classes.root}>
        <CssBaseline />
        <AppNavBar />
        <AppDrawer open={drawerOpen} />
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: drawerOpen,
          })}
        >
          <div className={classes.drawerHeader} />

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/404" component={NotFound} />
            <Route path="/under-development" component={UnderDevelopment} />
            <Route path="/institutes">
              <InstituteRoutes />
            </Route>
            <Route path="/departments">
              <Departments />
            </Route>
            <Route path="/groups">
              <Groups />
            </Route>
            <Route path="/students">
              <Students />
            </Route>
            <Route path="/subjects">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/lessons">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/attendance">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/grades">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/reports">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/import">
              <Import />
            </Route>
            <Route path="/settings">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/about">
              <Redirect to="/under-development" />
            </Route>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/login" exact>
              <Login />
            </Route>
            <Redirect to="/404" />
          </Switch>
        </main>
      </div>
    </div>
  );
};

export default Layout;
