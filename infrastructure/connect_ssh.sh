#!/usr/bin/env bash

apt update && apt install openssh-client -y -qq
eval $(ssh-agent -s)
mkdir -p ~/.ssh
echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
echo "$SSH_SERVER_HOSTKEY" > ~/.ssh/known_hosts
chmod 700 ~/.ssh