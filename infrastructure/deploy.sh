#!/usr/bin/env bash

scp -v docker-compose.yml gradeo@$SERVER_URL:gradeo/docker-compose.yml
ssh -v -t gradeo@$SERVER_URL "cd gradeo && docker-compose stop && docker-compose pull && docker-compose up -d"