create table assessments
(
    id           serial    not null,
    score        integer   not null,
    task_id      int       not null,
    student_id   int       not null,
    created_date timestamp not null,
    removed_date timestamp null,
    updated_date timestamp null,
    primary key (id)
);
