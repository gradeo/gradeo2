create table groups
(
    id               serial    not null,
    formed_date      date,
    name             varchar(100),
    number_group     varchar(100),
    department_id    int,
    created_date     timestamp not null,
    removed_date     timestamp null,
    updated_date     timestamp null,
    primary key (id)
);

