alter table groups
    add column teacher_id int;

alter table groups
    add foreign key (teacher_id) references teacher (id);
