create table institutes
(
    id           serial       not null,
    name         varchar(100) not null,
    created_date timestamp    not null,
    updated_date timestamp    null,
    removed_date timestamp    null,
    primary key (id)
);