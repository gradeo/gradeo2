alter table teacher
    add column user_id int;

alter table teacher
    add foreign key (user_id) references users (id);

alter table teacher
    add column subject_id int;

alter table teacher
    add foreign key (subject_id) references subjects (id);