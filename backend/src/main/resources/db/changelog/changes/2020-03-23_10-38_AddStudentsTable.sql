create table students
(
    id               serial       not null,
    name             varchar(100) not null,
    group_id         int          not null,
    grade_book       int          not null,
    enrollment_date  date,
    deduction_date   date,
    educational_form varchar(50)  not null,
    created_date     timestamp    not null,
    updated_date     timestamp    null,
    removed_date     timestamp    null,
    primary key (id),
    foreign key (group_id) references groups (id)
);



