-- удаление колонки, созданой по ошибке
alter table teacher
    drop column teacher_id;

-- удаление курируемой группы
alter table teacher
    drop column group_id;

--удаление из группы столбца для куратора
alter table groups
    drop column teacher_id;