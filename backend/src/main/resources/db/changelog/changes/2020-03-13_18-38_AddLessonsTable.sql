create table lessons
(
    id           serial       not null,
    name         varchar(100) not null,
    subject_id   int          not null,
    group_id     int          not null,
    teacher_id   int /*not null*/,
    auditorium   varchar(50)  not null,
    date         timestamp    not null,
    created_date timestamp    not null,
    updated_date timestamp    null,
    removed_date timestamp    null,
    primary key (id),
    foreign key (group_id) references groups (id)
);



