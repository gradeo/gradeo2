create table attendance
(
    id                serial not null,
    lesson_id         int    not null,
    student_id        int    not null,
    attendance_status varchar(50),
    created_date      timestamp without time zone,
    removed_date      timestamp without time zone,
    updated_date      timestamp without time zone,
    primary key (id)
);
