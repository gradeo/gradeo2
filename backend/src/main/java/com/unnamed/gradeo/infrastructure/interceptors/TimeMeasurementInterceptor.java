package com.unnamed.gradeo.infrastructure.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//данный класс логирует время до обработки, после обработки или после завершения запросов
public class TimeMeasurementInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory
            .getLogger(TimeMeasurementInterceptor.class);

    //вызывается до обработки запроса
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler)
            throws Exception {
        //фиксирует время до обработки запроса и сохраняет его в request
        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);
        return true;
    }

    //вызывается перед отправкой ответа клиенту
    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception ex)
            throws Exception {
        //достает время начала обработки и считает разницу с текущим
        long startTime = (Long) request.getAttribute("startTime");
        logger.debug("Request URL: " + request.getRequestURL().toString()
                + ":: Time Elapsed = " + (System.currentTimeMillis() - startTime) + " ms");
    }
}
