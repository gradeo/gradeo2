package com.unnamed.gradeo.infrastructure.exceptionHandlers;

import com.unnamed.gradeo.core.dtos.util.ApiError;
import com.unnamed.gradeo.core.exceptions.EntityAlreadyExistsException;
import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;
import com.unnamed.gradeo.core.exceptions.ImportIOException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<Object> handleEntityNotFound(
            EntityNotFoundException ex) {
        var apiError = new ApiError(
                HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), ex.getError());
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler({EntityAlreadyExistsException.class})
    public ResponseEntity<Object> handleEntityAlreadyExists(
            EntityAlreadyExistsException ex) {
        var apiError = new ApiError(
                HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ex.getError());
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler({ImportIOException.class})
    public ResponseEntity<Object> handleImportIOException(
            ImportIOException ex) {
        var apiError = new ApiError(
                HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ex.getError());
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<Object> handleMultipartException(
            MultipartException ex) {
        var apiError = new ApiError(
                HttpStatus.PAYLOAD_TOO_LARGE, ex.getLocalizedMessage(), ex.getMessage());
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
