package com.unnamed.gradeo.controllers;

import an.awesome.pipelinr.Pipeline;
import com.unnamed.gradeo.core.dtos.bakers.BakerViewDto;
import com.unnamed.gradeo.core.features.bakers.view.GetBakersQuery;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/bakers")
@AllArgsConstructor
public class BakersApiController {

    private final Pipeline pipeline;

    @GetMapping
    public List<BakerViewDto> getAllBakers() {
        return pipeline.send(new GetBakersQuery());
    }
}
