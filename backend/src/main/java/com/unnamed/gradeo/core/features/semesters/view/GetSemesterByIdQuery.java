package com.unnamed.gradeo.core.features.semesters.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.semesters.SemesterViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetSemesterByIdQuery implements Command<SemesterViewDto> {

    @NotNull(message = "Выберите семестр")
    @Positive
    private Integer id;
}
