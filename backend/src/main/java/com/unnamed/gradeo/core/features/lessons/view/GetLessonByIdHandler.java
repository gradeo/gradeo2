package com.unnamed.gradeo.core.features.lessons.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.lessons.LessonViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchLessonException;
import com.unnamed.gradeo.repositories.LessonsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetLessonByIdHandler implements Command.Handler<GetLessonByIdQuery, LessonViewDto> {

    private final LessonsRepository repository;
    private final ModelMapper mapper;

    @Override
    public LessonViewDto handle(GetLessonByIdQuery command) {
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchLessonException::new);
        return mapper.map(entity, LessonViewDto.class);
    }
}
