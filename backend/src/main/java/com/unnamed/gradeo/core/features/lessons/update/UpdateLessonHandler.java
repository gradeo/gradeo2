package com.unnamed.gradeo.core.features.lessons.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchLessonException;
import com.unnamed.gradeo.repositories.LessonsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateLessonHandler implements Command.Handler<UpdateLessonCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final LessonsRepository repository;

    @Override
    public Voidy handle(UpdateLessonCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Lesson", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchLessonException::new);

        entity.setDate(LocalDateTime.of(command.getDate(), command.getTime()));
        entity.setAuditorium(command.getAuditorium());
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

}
