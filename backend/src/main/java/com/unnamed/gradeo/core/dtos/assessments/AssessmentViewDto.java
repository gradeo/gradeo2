package com.unnamed.gradeo.core.dtos.assessments;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AssessmentViewDto {
    private Integer id;

    private Integer studentId;

    private Integer taskId;

    private Integer subjectId;

    private String studentName;

    private String taskName;

    private String taskType;

    private String subjectName;

    private Integer score;
}
