package com.unnamed.gradeo.core.features.institutes.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchInstituteException;
import com.unnamed.gradeo.repositories.InstituteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
//для удаления института
public class DeleteInstituteHandler implements Command.Handler<DeleteInstituteCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final InstituteRepository repository;

    public DeleteInstituteHandler(InstituteRepository repository) {
        this.repository = repository;
    }

    @Override
    public Voidy handle(DeleteInstituteCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Institute", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchInstituteException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
