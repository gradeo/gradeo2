package com.unnamed.gradeo.core.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс-сущность с данными разработчиков проекта.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Baker {
    private String name;
    private String role;
    private String email;
}
