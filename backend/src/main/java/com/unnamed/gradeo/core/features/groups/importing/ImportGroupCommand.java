package com.unnamed.gradeo.core.features.groups.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImportGroupCommand implements Command<Voidy> {
    @NotNull(message = "Введите файл")
    private MultipartFile file;

}
