package com.unnamed.gradeo.core.features.groups.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateGroupCommand implements Command<Voidy> {
    @NotNull(message = "Не выбран id сущности")
    @Positive
    private Integer id;
    @NotBlank(message = "Введите новое название группы")
    private String name;
    @NotNull(message = "Выберите кафедру")
    @Positive
    private Integer departmentId;
    @NotNull(message = "Введите дату образования группы")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate formedDate;

}
