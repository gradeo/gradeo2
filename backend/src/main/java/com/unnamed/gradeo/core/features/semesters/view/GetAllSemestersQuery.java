package com.unnamed.gradeo.core.features.semesters.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.semesters.SemesterViewDto;

import java.util.Collection;
import java.util.List;

public class GetAllSemestersQuery implements Command<List<SemesterViewDto>> {
}
