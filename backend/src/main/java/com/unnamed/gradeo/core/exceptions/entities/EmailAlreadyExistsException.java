package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.BusinessLogicException;

public class EmailAlreadyExistsException extends BusinessLogicException {
}
