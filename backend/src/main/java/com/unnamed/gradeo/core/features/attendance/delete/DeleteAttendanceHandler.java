package com.unnamed.gradeo.core.features.attendance.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchAttendanceException;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteAttendanceHandler implements Command.Handler<DeleteAttendanceCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AttendanceRepository repository;

    @Override
    public Voidy handle(DeleteAttendanceCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Attendance", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchAttendanceException::new);

        entity.setRemovedDate(LocalDateTime.now());
        repository.save(entity);
        return new Voidy();
    }
}
