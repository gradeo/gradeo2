package com.unnamed.gradeo.core.features.semesters.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.entities.Student;
import com.unnamed.gradeo.core.entities.Subject;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSemesterException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.SemestersRepository;
import com.unnamed.gradeo.repositories.StudentsRepository;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;

@Component
@AllArgsConstructor
public class UpdateSemesterHandler implements Command.Handler<UpdateSemesterCommand, Voidy> {

    private final SemestersRepository repository;
    private final StudentsRepository studentsRepository;
    private final SubjectsRepository subjectsRepository;

    @Override
    public Voidy handle(UpdateSemesterCommand command) {
        var semester = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSemesterException::new);
        var students = new HashSet<Student>();
        for(int studentId : command.getStudentsId()){
            students.add(
                    studentsRepository
                        .findByIdAndRemovedDateIsNull(studentId)
                        .orElseThrow(NoSuchStudentException::new)
            );
        }
        semester.setStudents(students);
        semester.setStartDate(command.getStartDate());
        semester.setEndDate(command.getEndDate());
        var subjects = new HashSet<Subject>();
        for (int subjectId : command.getSubjectsId()){
            subjects.add(
                    subjectsRepository
                        .findByIdAndRemovedDateIsNull(subjectId)
                        .orElseThrow(NoSuchSubjectException::new)
            );
        }
        semester.setSubjects(subjects);
        semester.setUpdatedDate(LocalDateTime.now());
        repository.save(semester);
        return new Voidy();
    }
}
