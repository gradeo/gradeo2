package com.unnamed.gradeo.core.features.keycloak.users.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserCommand implements Command<Voidy> {
    @Size(min = 2, message = "Введите никнейм")
    private String username;

    @NotNull(message = "Введите пароль")
    private String password;

    @NotNull(message = "Введите адрес электронной почты")
    private String email;

    @NotNull(message = "Аккаунт активен?")
    private Boolean enabled;
}
