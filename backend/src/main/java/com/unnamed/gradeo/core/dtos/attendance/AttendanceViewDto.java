package com.unnamed.gradeo.core.dtos.attendance;

import com.unnamed.gradeo.core.entities.AttendanceStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AttendanceViewDto {
    private Integer id;

    private Integer subjectId;

    private Integer lessonId;

    private Integer groupId;

    private Integer studentId;

    private Integer semesterId;

    private String subjectName;

    private String lessonName;

    private String groupName;

    private String studentName;

    private Integer semesterNumber;

    private boolean attendanceStatus;

    public void setAttendanceStatus(AttendanceStatus attendanceStatus) {
        switch (attendanceStatus) {
            case PRESENCE:
                this.attendanceStatus = true;
                break;
            case ABSENCE:
                this.attendanceStatus = false;
                break;
        }
    }
}
