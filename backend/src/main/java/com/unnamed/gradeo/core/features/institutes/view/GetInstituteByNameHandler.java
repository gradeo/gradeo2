package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchInstituteException;
import com.unnamed.gradeo.repositories.InstituteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class GetInstituteByNameHandler implements Command.Handler<GetInstituteByNameQuery, InstituteViewDto> {

    private final InstituteRepository repository;
    private final ModelMapper mapper;

    public GetInstituteByNameHandler(@NotNull InstituteRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public InstituteViewDto handle(GetInstituteByNameQuery command) {
        var entity = repository
                .findByNameAndRemovedDateIsNull(command.getName())
                .orElseThrow(NoSuchInstituteException::new);
        return mapper.map(entity, InstituteViewDto.class);
    }
}
