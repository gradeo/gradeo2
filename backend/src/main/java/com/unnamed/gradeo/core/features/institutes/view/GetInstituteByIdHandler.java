package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchInstituteException;
import com.unnamed.gradeo.repositories.InstituteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class GetInstituteByIdHandler implements Command.Handler<GetInstituteByIdQuery, InstituteViewDto> {

    private final InstituteRepository repository;
    private final ModelMapper mapper;

    public GetInstituteByIdHandler(@NotNull InstituteRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public InstituteViewDto handle(GetInstituteByIdQuery command) {
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchInstituteException::new);
        return mapper.map(entity, InstituteViewDto.class);
    }
}
