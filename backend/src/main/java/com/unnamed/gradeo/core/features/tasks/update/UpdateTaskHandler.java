package com.unnamed.gradeo.core.features.tasks.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchTaskException;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateTaskHandler implements Command.Handler<UpdateTaskCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository subjectsRepository;
    private final TaskRepository repository;

    @Override
    public Voidy handle(UpdateTaskCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Task", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchTaskException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getSubjectId());
        var subject = subjectsRepository
                .findByIdAndRemovedDateIsNull(command.getSubjectId())
                .orElseThrow(NoSuchSubjectException::new);

        entity.setSubject(subject);
        entity.setName(command.getName());
        entity.setType(command.getType());
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
