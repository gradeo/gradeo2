package com.unnamed.gradeo.core.features.attendance.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.entities.Attendance;
import com.unnamed.gradeo.core.entities.AttendanceStatus;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateAttendanceHandler implements Command.Handler<UpdateAttendanceCommand, Voidy> {

    private final AttendanceRepository repository;

    /**
     * Редактирование посещаемости.
     * Логика: Для каждой существующей сущности посещаемости
     * 1) Если studentsId в command = 0 (никто не пришел на занятие), всем студентам ставим статус "Отсутствовал"
     * 2) Иначе: Для каждого id студента в studentsId в command:
     * 2.1) Если id = id в сущности посещаемости, присваем этому студенту статус "Присутствовал"
     * 2.2) Иначе присваеваем этому студенту статус "Отсутствовал"
     *
     * @param command - объект с данными
     */

    @Override
    public Voidy handle(UpdateAttendanceCommand command) {
        var attendance = repository
                .findAllByLessonIdAndRemovedDateIsNull(command.getLessonId());

        if (command.getStudentsId().length == 0) {
            setAllAbsence(attendance);
        } else {
            for (Attendance presenceAttendance : attendance) {
                for (int presenceStudentId : command.getStudentsId()) {
                    if (presenceAttendance.getStudent().getId() == presenceStudentId) {
                        presenceAttendance.setAttendanceStatus(AttendanceStatus.PRESENCE);
                        presenceAttendance.setUpdatedDate(LocalDateTime.now());
                        repository.save(presenceAttendance);
                        break;
                    } else {
                        presenceAttendance.setAttendanceStatus(AttendanceStatus.ABSENCE);
                        presenceAttendance.setUpdatedDate(LocalDateTime.now());
                        repository.save(presenceAttendance);
                    }
                }
            }
        }
        return new Voidy();
    }

    private void setAllAbsence(Iterable<Attendance> attendances) {
        for (Attendance absenceAttendance : attendances) {
            absenceAttendance.setAttendanceStatus(AttendanceStatus.ABSENCE);
            absenceAttendance.setUpdatedDate(LocalDateTime.now());
            repository.save(absenceAttendance);
        }
    }
}
