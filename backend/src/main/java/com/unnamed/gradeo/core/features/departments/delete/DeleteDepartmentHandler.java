package com.unnamed.gradeo.core.features.departments.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DeleteDepartmentHandler implements Command.Handler<DeleteDepartmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository repository;

    public DeleteDepartmentHandler(DepartmentsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Voidy handle(DeleteDepartmentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchDepartmentException::new);
        logger.debug("Setting removedDate");
        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
