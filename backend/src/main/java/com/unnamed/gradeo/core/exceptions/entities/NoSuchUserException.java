package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchUserException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "User is not found";
    }
}
