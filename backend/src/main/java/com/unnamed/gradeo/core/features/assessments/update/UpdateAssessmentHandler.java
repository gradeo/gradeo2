package com.unnamed.gradeo.core.features.assessments.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchAssessmentException;
import com.unnamed.gradeo.repositories.AssessmentsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateAssessmentHandler implements Command.Handler<UpdateAssessmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AssessmentsRepository repository;

    @Override
    public Voidy handle(UpdateAssessmentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Assessment", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchAssessmentException::new);

        entity.setScore(command.getScore());
        entity.setUpdatedDate(LocalDateTime.now());
        repository.save(entity);
        return new Voidy();
    }
}
