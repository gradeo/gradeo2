package com.unnamed.gradeo.core.features.students.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.abstractions.ImportUtil;
import com.unnamed.gradeo.core.exceptions.ImportIOException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

@Slf4j
@Component
@AllArgsConstructor
public class ImportStudentHandler implements Command.Handler<ImportStudentCommand, Voidy> {

    private final ImportUtil importLib;

    @Override
    public Voidy handle(ImportStudentCommand command) {
        try {
            importStudents(command.getFile());
        } catch (IOException e) {
            throw new ImportIOException();
        }
        return new Voidy();
    }

    private void importStudents(MultipartFile file) throws IOException {
        var workbook = importLib.checkType(file);
        var sheet = workbook.getSheetAt(0);
        var rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) rowIterator.next();

        Map<Integer, String> instituteCache = importLib.getInstituteCache();
        Map<Integer, String> departmentCache = importLib.getDepartmentCache();
        Map<Integer, String> groupCache = importLib.getGroupCache();
        Set<Integer> studentCache = importLib.getStudentsCache();

        while (rowIterator.hasNext()) {
            var cellIterator = rowIterator.next().cellIterator();
            var instituteName = cellIterator.next().getStringCellValue();
            var departmentName = cellIterator.next().getStringCellValue();
            var groupName = cellIterator.next().getStringCellValue();
            var studentName = cellIterator.next().getStringCellValue();
            var educationalForm = cellIterator.next().getStringCellValue();
            var gradeBook = (int) cellIterator.next().getNumericCellValue();

            if (studentCache.contains(gradeBook)) {
                log.debug("Попытка импорта студента '{}' - студент уже существует.", studentName);
            } else {
                // если в кэше нет необходимой группы
                if (!groupCache.containsValue(groupName)) {
                    // если в кэше нет необходимой кафедры
                    if (!departmentCache.containsValue(departmentName)) {
                        //если в кэше нет необходимого института
                        if (!instituteCache.containsValue(instituteName)) {
                            importLib.createInstitute(instituteCache, instituteName);
                        }
                        importLib.createDepartment(departmentCache,
                                departmentName, instituteName);
                    }
                    importLib.createGroup(groupCache,
                            groupName, departmentName);
                }
                // затем создаем студента
                importLib.createStudent(studentCache,
                        studentName, groupName, educationalForm, gradeBook);
            }

        }
    }


}
