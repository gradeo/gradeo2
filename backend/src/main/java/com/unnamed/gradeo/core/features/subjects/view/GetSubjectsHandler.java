package com.unnamed.gradeo.core.features.subjects.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.subjects.SubjectViewDto;
import com.unnamed.gradeo.core.entities.Subject;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetSubjectsHandler implements Command.Handler<GetSubjectsQuery, List<SubjectViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository repository;
    private final ModelMapper mapper;

    @Override
    public List<SubjectViewDto> handle(GetSubjectsQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Subjects");
        var subjects = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(subjects.spliterator(), false)
                .map(entity -> mapper.map(entity, SubjectViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void configureMapping() {
        this.mapper.typeMap(Subject.class, SubjectViewDto.class)
                .addMappings(mapper -> mapper.map(Subject::getAllTeachersNames, SubjectViewDto::setTeachersNames))
                .addMappings(mapper -> mapper.map(Subject::getAllTeachersId, SubjectViewDto::setTeachersId))
                .addMappings(mapper -> mapper.map(s -> s.getDepartment().getName(), SubjectViewDto::setDepartmentName))
                .addMappings(mapper -> mapper.map(s -> s.getDepartment().getInstitute().getName(), SubjectViewDto::setInstituteName))
                .addMappings(mapper -> mapper.map(s -> s.getDepartment().getId(), SubjectViewDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(s -> s.getDepartment().getInstitute().getId(), SubjectViewDto::setInstituteId));
    }
}
