package com.unnamed.gradeo.core.features.students.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Student;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSemesterException;
import com.unnamed.gradeo.repositories.SemestersRepository;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateStudentHandler implements Command.Handler<CreateStudentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final SemestersRepository semestersRepository;
    private final StudentsRepository repository;

    @Override
    public Voidy handle(CreateStudentCommand command) {
        logger.debug("Mapping to Student.class");
        var entity = mapper.map(command, Student.class);
        logger.debug(LogStrings.FIND_BY_ID, "Semester", command.getSemesterId());
        var semester = semestersRepository
                .findByIdAndRemovedDateIsNull(command.getSemesterId())
                .orElseThrow(NoSuchSemesterException::new);
        entity.setGradeBook(command.getGradeBook());
        entity.setSemester(semester);
        entity.setEnrollmentDate(command.getEnrollmentDate());
        entity.setEducationalForm(command.getEducationalForm());
        entity.setCreatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
