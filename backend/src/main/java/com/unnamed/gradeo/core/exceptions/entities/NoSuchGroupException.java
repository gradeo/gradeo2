package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchGroupException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Group is not found";
    }
}
