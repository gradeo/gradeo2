package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityAlreadyExistsException;

public class AssessmentAlreadyExistException extends EntityAlreadyExistsException {
    @Override
    public String getError() {
        return "Assessment already exists!";
    }
}
