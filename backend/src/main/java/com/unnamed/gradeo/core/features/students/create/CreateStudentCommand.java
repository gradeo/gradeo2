package com.unnamed.gradeo.core.features.students.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateStudentCommand implements Command<Voidy> {

    @Size(min = 5, message = "ФИО не должно быть пустым")
    private String name;

    @Positive
    @NotNull(message = "Введите правильный номер зачетной книжки")
    @Digits(integer = 8, fraction = 0, message = "Введите правильный номер зачетной книжки")
    private Integer gradeBook;

    @Positive
    @NotNull(message = "Выберите семестр")
    private Integer semesterId;

    @NotBlank(message = "Выберите форму обучения")
    private String educationalForm;

    //TODO: временное отключение
    //@NotNull(message = "Выберите дату зачисления")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate enrollmentDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deductionDate;


}
