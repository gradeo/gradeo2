package com.unnamed.gradeo.core.abstractions;

public interface EmailSender {
    void send(String email, String subject, String body);
}
