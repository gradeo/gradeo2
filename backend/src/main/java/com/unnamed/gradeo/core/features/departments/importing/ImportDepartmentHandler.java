package com.unnamed.gradeo.core.features.departments.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.abstractions.ImportUtil;
import com.unnamed.gradeo.core.exceptions.ImportIOException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class ImportDepartmentHandler implements Command.Handler<ImportDepartmentCommand, Voidy> {

    private final ImportUtil importLib;

    @Override
    public Voidy handle(ImportDepartmentCommand command) {
        try {
            importDepartments(command.getFile());
        } catch (IOException e) {
            throw new ImportIOException();
        }
        return new Voidy();
    }

    private void importDepartments(MultipartFile file) throws IOException {
        var workbook = importLib.checkType(file);
        var sheet = workbook.getSheetAt(0);
        var rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) rowIterator.next();

        Map<Integer, String> instituteCache = importLib.getInstituteCache();
        Map<Integer, String> departmentCache = importLib.getDepartmentCache();

        while (rowIterator.hasNext()) {
            var cellIterator = rowIterator.next().cellIterator();
            var instituteName = cellIterator.next().getStringCellValue();
            var departmentName = cellIterator.next().getStringCellValue();

            if (departmentCache.containsValue(departmentName)) {
                log.debug("Попытка импорта кафедры '{}' - кафедра уже существует.", departmentName);
            } else {
                // если в кэше нет необходимого института
                if (!instituteCache.containsValue(instituteName)) {
                    importLib.createInstitute(instituteCache, instituteName);
                }
                // затем создаем кафедру
                importLib.createDepartment(departmentCache,
                        departmentName, instituteName);
            }
        }
    }
}
