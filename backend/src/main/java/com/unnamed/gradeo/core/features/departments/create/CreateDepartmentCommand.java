package com.unnamed.gradeo.core.features.departments.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateDepartmentCommand implements Command<Voidy> {
    @Size(min = 2, message = "Введите название кафедры")
    private String name;

    @Positive
    @NotNull(message = "Выберите институт")
    private Integer instituteId;
}
