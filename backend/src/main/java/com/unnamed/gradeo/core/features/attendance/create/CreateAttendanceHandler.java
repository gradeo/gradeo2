package com.unnamed.gradeo.core.features.attendance.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.entities.Attendance;
import com.unnamed.gradeo.core.entities.AttendanceStatus;
import com.unnamed.gradeo.core.entities.Student;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchLessonException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import com.unnamed.gradeo.repositories.LessonsRepository;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CreateAttendanceHandler implements Command.Handler<CreateAttendanceCommand, Voidy> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final StudentsRepository studentsRepository;
    private final LessonsRepository lessonsRepository;
    private final AttendanceRepository repository;

    /**
     * Создание посещения одного занятия.
     * Логика:
     * 1) Получаем занятие по id
     * 2) Получаем список всех студентов в группе
     * 3) Присваиваем всем студентам, чьи id есть в studentsId в command, занятие и статус "Присутствовал"
     * 3.1) Удаляем их из списка
     * 4) Оставшимся студентам в списке присваиваем занятие и статус "Отсутствовал"
     *
     * @param command - объект с данными
     * @throws NoSuchLessonException  - обработка в AttendanceController
     * @throws NoSuchStudentException - обработка в AttendanceController
     */
    @Override
    public Voidy handle(CreateAttendanceCommand command) {
        logger.debug("Mapping to Attendance.class");
        var lesson = lessonsRepository
                .findByIdAndRemovedDateIsNull(command.getLessonId())
                .orElseThrow(NoSuchLessonException::new);

        var studentsInSemester = studentsRepository
                .findAllBySemesterIdAndRemovedDateIsNull(command.getSemesterId());

        for (int presenceStudentId : command.getStudentsId()) {
            var student = studentsRepository
                    .findByIdAndRemovedDateIsNull(presenceStudentId)
                    .orElseThrow(NoSuchStudentException::new);

            var presenceEntity = mapper.map(command, Attendance.class);
            studentsInSemester.remove(student);
            presenceEntity.setStudent(student);
            presenceEntity.setLesson(lesson);
            presenceEntity.setAttendanceStatus(AttendanceStatus.PRESENCE);
            presenceEntity.setCreatedDate(LocalDateTime.now());
            logger.info(LogStrings.SAVE, presenceEntity.getId(), presenceEntity.getStudent());
            repository.save(presenceEntity);
        }

        for (Student student : studentsInSemester) {
            var absenceEntity = mapper.map(command, Attendance.class);
            absenceEntity.setStudent(student);
            absenceEntity.setLesson(lesson);
            absenceEntity.setAttendanceStatus(AttendanceStatus.ABSENCE);
            absenceEntity.setCreatedDate(LocalDateTime.now());
            logger.info(LogStrings.SAVE, absenceEntity.getId(), absenceEntity.getStudent());
            repository.save(absenceEntity);
        }
        return new Voidy();
    }
}
