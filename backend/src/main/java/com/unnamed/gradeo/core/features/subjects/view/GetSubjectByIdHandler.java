package com.unnamed.gradeo.core.features.subjects.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.subjects.SubjectViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSubjectException;
import com.unnamed.gradeo.repositories.SubjectsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetSubjectByIdHandler implements Command.Handler<GetSubjectByIdQuery, SubjectViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SubjectsRepository repository;
    private final ModelMapper mapper;

    @Override
    public SubjectViewDto handle(GetSubjectByIdQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Subject", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSubjectException::new);

        return mapper.map(entity, SubjectViewDto.class);
    }
}
