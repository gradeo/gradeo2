package com.unnamed.gradeo.core.features.semesters.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.semesters.SemesterViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSemesterException;
import com.unnamed.gradeo.repositories.SemestersRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetSemesterByIdHandler implements Command.Handler<GetSemesterByIdQuery, SemesterViewDto> {

    private final SemestersRepository repository;
    private final ModelMapper mapper;

    @Override
    public SemesterViewDto handle(GetSemesterByIdQuery command) {
        var semester = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchSemesterException::new);
        return mapper.map(semester,SemesterViewDto.class); }
}
