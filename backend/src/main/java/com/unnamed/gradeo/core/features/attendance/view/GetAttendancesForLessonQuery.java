package com.unnamed.gradeo.core.features.attendance.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Getter
@AllArgsConstructor
public class GetAttendancesForLessonQuery implements Command<List<AttendanceViewDto>> {
    @Positive
    @NotNull(message = "Не выбран id сущности")
    private Integer lessonId;
}
