package com.unnamed.gradeo.core.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntityAlreadyExistsException extends BusinessLogicException {

    public EntityAlreadyExistsException() {
        super();
    }

    public String getError() {
        return "Entity already exists!";
    }

    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
