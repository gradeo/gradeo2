package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchAttendanceException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Attendance is not found";
    }
}
