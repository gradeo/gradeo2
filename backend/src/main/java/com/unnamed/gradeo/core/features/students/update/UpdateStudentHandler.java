package com.unnamed.gradeo.core.features.students.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchSemesterException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.repositories.SemestersRepository;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class UpdateStudentHandler implements Command.Handler<UpdateStudentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SemestersRepository semestersRepository;
    private final StudentsRepository repository;

    @Override
    public Voidy handle(UpdateStudentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Student", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchStudentException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Semester", command.getSemesterId());
        var semester = semestersRepository
                .findByIdAndRemovedDateIsNull(command.getSemesterId())
                .orElseThrow(NoSuchSemesterException::new);

        entity.setName(command.getName());
        entity.setGradeBook(command.getGradeBook());
        entity.setSemester(semester);
        entity.setEnrollmentDate(command.getEnrollmentDate());
        entity.setDeductionDate(command.getDeductionDate());
        entity.setEducationalForm(command.getEducationalForm());
        entity.setUpdatedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
