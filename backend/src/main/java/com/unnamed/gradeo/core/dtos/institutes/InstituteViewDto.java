package com.unnamed.gradeo.core.dtos.institutes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InstituteViewDto {
    private int id;
    private String name;

    // TODO: Calculate departmentsCount in handler
    private int departmentsCount = 3;
}
