package com.unnamed.gradeo.core.features.middleware;

import an.awesome.pipelinr.Command;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Order(1)
//логер работает только с запросами
class Loggable implements Command.Middleware {

    @Override
    public <R, C extends Command<R>> R invoke(C command, Next<R> next) {
        //лог запроса
        log.debug(command.toString());
        R response = next.invoke();
        //лог результата
        if (response != null) log.debug(response.toString());
        else
            log.debug("Response == null");
        return response;
    }


}