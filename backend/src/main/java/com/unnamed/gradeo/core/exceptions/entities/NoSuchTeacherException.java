package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchTeacherException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Teacher is not found";
    }
}
