package com.unnamed.gradeo.core.entities;

/**
 * Класс-перечисление должности преподавателя
 * Ассистент, старший преподаватель, доцент, профессор,
 */
public enum Position {
    ASSISTANT,
    SENIOR_LECTURER,
    ASSISTANT_PROFESSOR,
    PROFESSOR
}
