package com.unnamed.gradeo.core.features.students.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImportStudentCommand implements Command<Voidy> {
    @NotNull(message = "Введите файл")
    private MultipartFile file;

}
