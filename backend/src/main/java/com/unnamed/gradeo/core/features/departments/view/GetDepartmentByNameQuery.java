package com.unnamed.gradeo.core.features.departments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class GetDepartmentByNameQuery implements Command<DepartmentViewDto> {

    @NotNull(message = "Введите имя кафедры")
    private String name;
}
