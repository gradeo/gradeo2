package com.unnamed.gradeo.core.entities;

/**
 * Класс-перечисление типов статуса посещаемости.
 */
public enum AttendanceStatus {
    PRESENCE,
    ABSENCE,
    LATE
}
