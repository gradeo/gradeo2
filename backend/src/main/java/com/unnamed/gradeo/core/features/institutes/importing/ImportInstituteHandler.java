package com.unnamed.gradeo.core.features.institutes.importing;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.abstractions.ImportUtil;
import com.unnamed.gradeo.core.exceptions.ImportIOException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
public class ImportInstituteHandler implements Command.Handler<ImportInstituteCommand, Voidy> {

    private final ImportUtil importLib;

    @Override
    public Voidy handle(ImportInstituteCommand command) {
        try {
            importInstitutes(command.getFile());
        } catch (IOException e) {
            throw new ImportIOException();
        }
        return new Voidy();
    }

    private void importInstitutes(MultipartFile file) throws IOException {
        // проверка типа файла - Excel 97-2003 или 2007+
        var workbook = importLib.checkType(file);
        var sheet = workbook.getSheetAt(0);
        // создаем итератор для листа
        var rowIterator = sheet.iterator();
        // проверяем на пустоту -> пропускаем шапку листа
        if (rowIterator.hasNext()) rowIterator.next();
        // подключаем кэш
        Map<Integer, String> instituteCache = importLib.getInstituteCache();
        // пока есть строка ниже
        while (rowIterator.hasNext()) {
            // перемещаем итератор на ячейку ниже
            var cellIterator = rowIterator.next().cellIterator();
            // читаем с неё
            var instituteName = cellIterator.next().getStringCellValue();
            // если в кэше есть совпадения
            if (instituteCache.containsValue(instituteName)) {
                log.info("Попытка импорта института '{}' - институт уже существует.", instituteName);
            } else {
                importLib.createInstitute(instituteCache, instituteName);
            }
        }
    }

}
