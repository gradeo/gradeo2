package com.unnamed.gradeo.core.features.students.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.students.StudentViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchStudentException;
import com.unnamed.gradeo.repositories.StudentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetStudentByNameHandler implements Command.Handler<GetStudentByNameQuery, StudentViewDto> {

    private final StudentsRepository repository;
    private final ModelMapper mapper;

    @Override
    public StudentViewDto handle(GetStudentByNameQuery command) {
        var entity = repository
                .findByNameAndRemovedDateIsNull(command.getName())
                .orElseThrow(NoSuchStudentException::new);
        return mapper.map(entity, StudentViewDto.class);
    }
}
