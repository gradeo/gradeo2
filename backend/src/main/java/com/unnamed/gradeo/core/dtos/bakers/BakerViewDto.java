package com.unnamed.gradeo.core.dtos.bakers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BakerViewDto {
    private String name;
    private String role;
    private String email;
}
