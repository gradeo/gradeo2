package com.unnamed.gradeo.core.features.subjects.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSubjectCommand implements Command<Voidy> {
    @Size(min = 2, message = "Введите название предмета")
    private String name;

    @Positive
    @NotNull(message = "Выберите кафедру")
    private Integer departmentId;

}