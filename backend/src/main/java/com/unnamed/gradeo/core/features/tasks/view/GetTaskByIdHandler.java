package com.unnamed.gradeo.core.features.tasks.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.tasks.TaskViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchTaskException;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetTaskByIdHandler implements Command.Handler<GetTaskByIdQuery, TaskViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final TaskRepository repository;
    private final ModelMapper mapper;

    @Override
    public TaskViewDto handle(GetTaskByIdQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Task", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchTaskException::new);

        return mapper.map(entity, TaskViewDto.class);
    }
}
