package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.ImportIOException;

public class UnsupportedImportFileType extends ImportIOException {
    @Override
    public String getError() {
        return "File type is not supported: Only .xls .xlsx";
    }
}
