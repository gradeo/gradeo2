package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class GetInstituteByNameQuery implements Command<InstituteViewDto> {

    @NotNull(message = "Введите имя института")
    private String name;
}
