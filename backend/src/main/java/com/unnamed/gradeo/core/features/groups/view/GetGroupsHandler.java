package com.unnamed.gradeo.core.features.groups.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import com.unnamed.gradeo.core.entities.Group;
import com.unnamed.gradeo.repositories.GroupsRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
//для возврата всех групп
public class GetGroupsHandler implements Command.Handler<GetGroupsQuery, List<GroupViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final GroupsRepository repository;

    public GetGroupsHandler(ModelMapper mapper, GroupsRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }


    @Override
    public List<GroupViewDto> handle(GetGroupsQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Groups");
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();
        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, GroupViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.typeMap(Group.class, GroupViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getId(), GroupViewDto::setInstituteId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getId(), GroupViewDto::setDepartmentId))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getInstitute().getName(), GroupViewDto::setInstituteName))
                .addMappings(mapper -> mapper.map(d -> d.getDepartment().getName(), GroupViewDto::setDepartmentName));
    }
}
