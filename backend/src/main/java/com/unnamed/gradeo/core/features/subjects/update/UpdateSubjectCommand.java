package com.unnamed.gradeo.core.features.subjects.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSubjectCommand implements Command<Voidy> {
    @NotNull(message = "Не выбран id сущности")
    @Positive
    private Integer id;

    @NotBlank(message = "Введите новое название предмета")
    private String name;

    @NotNull(message = "Выберите кафедру, к которой принадлежит предмет")
    @Positive
    private Integer departmentId;
}
