package com.unnamed.gradeo.core.features.tasks.delete;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchTaskException;
import com.unnamed.gradeo.repositories.TaskRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DeleteTaskHandler implements Command.Handler<DeleteTaskCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final TaskRepository repository;

    @Override
    public Voidy handle(DeleteTaskCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Task", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchTaskException::new);

        entity.setRemovedDate(LocalDateTime.now());
        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
