package com.unnamed.gradeo.core.features.semesters.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSemesterCommand implements Command<Voidy> {

    @Positive
    @NotNull(message = "Выберите семестр")
    private Integer id;

    @NotNull(message = "Выберите студентов")
    private Integer[] studentsId;

    @NotNull(message = "Выберете дату начала семестра")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = "Выберите дату конца семестра")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @NotNull(message = "Выберите студентов")
    private Integer[] subjectsId;
}
