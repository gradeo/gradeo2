package com.unnamed.gradeo.core.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EntityNotFoundException extends BusinessLogicException {

    public EntityNotFoundException() {
        super();
    }

    public String getError() {
        return "Entity not found";
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
