package com.unnamed.gradeo.core.features.groups.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchGroupException;
import com.unnamed.gradeo.repositories.GroupsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetGroupByNameHandler implements Command.Handler<GetGroupByNameQuery, GroupViewDto> {

    private final GroupsRepository repository;
    private final ModelMapper mapper;

    @Override
    public GroupViewDto handle(GetGroupByNameQuery command) {
        var entity = repository
                .findByNameAndRemovedDateIsNull(command.getName())
                .orElseThrow(NoSuchGroupException::new);
        return mapper.map(entity, GroupViewDto.class);
    }
}
