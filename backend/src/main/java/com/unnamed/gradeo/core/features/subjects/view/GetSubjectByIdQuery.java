package com.unnamed.gradeo.core.features.subjects.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.subjects.SubjectViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetSubjectByIdQuery implements Command<SubjectViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
