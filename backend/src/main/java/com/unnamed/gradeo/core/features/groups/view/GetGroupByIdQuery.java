package com.unnamed.gradeo.core.features.groups.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.groups.GroupViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetGroupByIdQuery implements Command<GroupViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
