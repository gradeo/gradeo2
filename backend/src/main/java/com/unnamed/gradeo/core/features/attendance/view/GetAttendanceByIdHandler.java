package com.unnamed.gradeo.core.features.attendance.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchAttendanceException;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetAttendanceByIdHandler implements Command.Handler<GetAttendanceByIdQuery, AttendanceViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final AttendanceRepository repository;
    private final ModelMapper mapper;

    @Override
    public AttendanceViewDto handle(GetAttendanceByIdQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Attendance", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchAttendanceException::new);

        return mapper.map(entity, AttendanceViewDto.class);
    }
}
