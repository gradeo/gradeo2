package com.unnamed.gradeo.core.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Entity
@Table(name = "reset_token")
@Getter
@Setter
public class ResetToken {
    private static final int EXPIRATION_DATE = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    private UUID token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    private Date expiryDate;

    private Boolean isUsed;

    public ResetToken() {
        expiryDate = Date.valueOf(LocalDate.now().plus(EXPIRATION_DATE, ChronoUnit.DAYS));
        isUsed = false;
        ;
    }
}
