package com.unnamed.gradeo.core.features.attendance.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.attendance.AttendanceViewDto;
import com.unnamed.gradeo.repositories.AttendanceRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetAttendancesForLessonHandler implements Command.Handler<GetAttendancesForLessonQuery, List<AttendanceViewDto>> {

    private final ModelMapper mapper;
    private final AttendanceRepository repository;

    @Override
    public List<AttendanceViewDto> handle(GetAttendancesForLessonQuery command) {
        var entities = repository.findAllByLessonIdAndRemovedDateIsNull(command.getLessonId());

        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, AttendanceViewDto.class))
                .collect(Collectors.toList());
    }

}
