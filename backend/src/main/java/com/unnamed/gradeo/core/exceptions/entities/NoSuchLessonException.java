package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchLessonException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Lesson is not found";
    }
}
