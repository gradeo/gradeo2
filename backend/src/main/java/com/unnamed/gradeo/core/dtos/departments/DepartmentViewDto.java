package com.unnamed.gradeo.core.dtos.departments;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DepartmentViewDto {
    private Integer id;
    private String name;
    private Integer instituteId;
    private String instituteName;
}
