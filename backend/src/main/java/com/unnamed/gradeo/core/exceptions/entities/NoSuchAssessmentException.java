package com.unnamed.gradeo.core.exceptions.entities;

import com.unnamed.gradeo.core.exceptions.EntityNotFoundException;

public class NoSuchAssessmentException extends EntityNotFoundException {
    @Override
    public String getError() {
        return "Assessment is not found";
    }
}
