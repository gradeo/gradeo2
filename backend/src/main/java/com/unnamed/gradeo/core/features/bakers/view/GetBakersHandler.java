package com.unnamed.gradeo.core.features.bakers.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.bakers.BakerViewDto;
import com.unnamed.gradeo.core.entities.Baker;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class GetBakersHandler implements Command.Handler<GetBakersQuery, List<BakerViewDto>> {

    private final ModelMapper mapper;
    private final List<Baker> bakers = Arrays.asList(
            new Baker("Денис Оболенский", "Team Leader", "denismaster@outlook.com"),
            new Baker("Максим Паньков", "Backend Dev", "msolntsevvv@gmail.com"),
            new Baker("Сергей Бондаренко", "Backend Dev", "serg1996bond@rambler.ru"),
            new Baker("Эмиль Ибраимов", "Frontend Dev", "emil2002.04.02@gmail.com"),
            new Baker("Андрей Юзвенко", "Frontend Dev", "dlya.yutuba.01@mail.ru"),
            new Baker("Андрей Сухих", "Frontend Dev", "sukhikh.andrew@gmail.com"),
            new Baker("Владимир Чернов", "DevOps", "iblackthefall@protonmail.com"),
            new Baker("Татьяна Петрова", "UI/UX", "i.love.sima@mail.ru")
    );


    @Override
    public List<BakerViewDto> handle(GetBakersQuery command) {
        return bakers.stream()
                .map(entity -> mapper.map(entity, BakerViewDto.class))
                .collect(Collectors.toList());
    }

}
