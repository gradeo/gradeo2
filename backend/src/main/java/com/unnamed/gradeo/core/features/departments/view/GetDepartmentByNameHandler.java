package com.unnamed.gradeo.core.features.departments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class GetDepartmentByNameHandler implements Command.Handler<GetDepartmentByNameQuery, DepartmentViewDto> {

    private final DepartmentsRepository repository;
    private final ModelMapper mapper;

    public GetDepartmentByNameHandler(@NotNull DepartmentsRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public DepartmentViewDto handle(GetDepartmentByNameQuery command) {
        var entity = repository
                .findByNameAndRemovedDateIsNull(command.getName())
                .orElseThrow(NoSuchDepartmentException::new);
        return mapper.map(entity, DepartmentViewDto.class);
    }
}
