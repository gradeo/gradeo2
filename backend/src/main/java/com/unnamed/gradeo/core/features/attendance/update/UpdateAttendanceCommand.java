package com.unnamed.gradeo.core.features.attendance.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateAttendanceCommand implements Command<Voidy> {
    @NotNull(message = "Не выбран id сущности")
    @Positive
    private Integer id;

    @Positive
    @NotNull(message = "Выберите занятие")
    private Integer lessonId;

    private Integer[] studentsId;
}
