package com.unnamed.gradeo.core.abstractions;

import com.unnamed.gradeo.core.entities.*;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchGroupException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchInstituteException;
import com.unnamed.gradeo.core.exceptions.entities.UnsupportedImportFileType;
import com.unnamed.gradeo.repositories.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class ImportUtil {

    private final InstituteRepository instituteRepository;
    private final DepartmentsRepository departmentsRepository;
    private final GroupsRepository groupsRepository;
    private final StudentsRepository studentsRepository;
    private final SemestersRepository semestersRepository;

    public Workbook checkType(MultipartFile file) throws IOException {
        if (file.getOriginalFilename().endsWith("xls")) {
            return new HSSFWorkbook(file.getInputStream());
        } else if (file.getOriginalFilename().endsWith("xlsx")) {
            return new XSSFWorkbook(file.getInputStream());
        } else {
            throw new UnsupportedImportFileType();
        }
    }

    public Map<Integer, String> getInstituteCache() {
        Map<Integer, String> cache = new HashMap<>();
        var entities = instituteRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        for (Institute i : entities) {
            cache.put(i.getId(), i.getName());
        }
        return cache;
    }

    public Map<Integer, String> getDepartmentCache() {
        Map<Integer, String> cache = new HashMap<>();
        var entities = departmentsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        for (Department d : entities) {
            cache.put(d.getId(), d.getName());
        }
        return cache;
    }

    public Map<Integer, String> getGroupCache() {
        Map<Integer, String> cache = new HashMap<>();
        var entities = groupsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        for (Group g : entities) {
            cache.put(g.getId(), g.getName());
        }
        return cache;
    }

    public Set<Integer> getStudentsCache() {
        Set<Integer> cache = new HashSet<>();
        var entities = studentsRepository.findAllByRemovedDateIsNullOrderByIdAsc();
        for (Student s : entities) {
            cache.add(s.getGradeBook());
        }
        return cache;
    }

    public void createInstitute(Map<Integer, String> instituteCache,
                                String instituteName) {
        var institute = new Institute();
        institute.setName(instituteName);
        institute.setCreatedDate(LocalDateTime.now());
        instituteCache.put(
                instituteRepository.save(institute).getId(),
                instituteName);
        log.info("Создание института '{}' - успешно.", instituteName);
    }

    public void createDepartment(Map<Integer, String> departmentCache,
                                 String departmentName, String instituteName) {
        var department = new Department();
        department.setName(departmentName);
        department.setInstitute(
                instituteRepository
                        .findByNameAndRemovedDateIsNull(instituteName)
                        .orElseThrow(NoSuchInstituteException::new));
        department.setCreatedDate(LocalDateTime.now());
        departmentCache.put(
                departmentsRepository.save(department).getId(),
                departmentName);
        log.info("Создание кафедры '{}' - успешно.", departmentName);
    }

    public void createGroup(Map<Integer, String> groupCache,
                            String groupName, String departmentName) {
        var group = new Group();
        group.setName(groupName);
        group.setDepartment(
                departmentsRepository
                        .findByNameAndRemovedDateIsNull(departmentName)
                        .orElseThrow(NoSuchDepartmentException::new));
        group.setCreatedDate(LocalDateTime.now());

        groupCache.put(
                groupsRepository.save(group).getId(),
                groupName);
        log.info("Создание группы '{}' - успешно.", groupName);

    }

    public void createStudent(Set<Integer> studentsCache,
                              String studentName, String groupName, String form, Integer gradeBook) {


        var semester = new Semester();
        semester.setGroup(groupsRepository
                .findByNameAndRemovedDateIsNull(groupName)
                .orElseThrow(NoSuchGroupException::new));
        semester.setNumber(1);
        semester.setCreatedDate(LocalDateTime.now());


        var student = new Student();

        student.setName(studentName);
        student.setSemester(semester);
        student.setEducationalForm(form);
        student.setGradeBook(gradeBook);
        student.setCreatedDate(LocalDateTime.now());

        semestersRepository.save(semester);
        studentsRepository.save(student);

        studentsCache.add(gradeBook);
        log.info("Создание студента '{}' - успешно.", studentName);
    }
}
