package com.unnamed.gradeo.core.dtos.lessons;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LessonViewDto {
    private Integer id;

    private String name;

    private Integer instituteId;

    private Integer departmentId;

    private Integer subjectId;

    private Integer groupId;

    private Integer semesterId;

    private String instituteName;

    private String departmentName;

    private String subjectName;

    private String groupName;

    private Integer semesterNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime date;

    private String auditorium;
}
