package com.unnamed.gradeo.core.features.assessments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.assessments.AssessmentViewDto;

import java.util.List;

public class GetAssessmentsQuery implements Command<List<AssessmentViewDto>> {
}
