package com.unnamed.gradeo.core.features.keycloak.users.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.core.exceptions.entities.UserAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.List;

@Slf4j
@Component
public class CreateUserHandler implements Command.Handler<CreateUserCommand, Voidy> {

    private RestTemplate clientAccessRestTemplate;
    private final ModelMapper mapper;

    public CreateUserHandler(RestTemplate clientAccessRestTemplate, ModelMapper mapper) {
        this.clientAccessRestTemplate = clientAccessRestTemplate;
        this.mapper = mapper;
    }

    @Value("${keycloak.auth-server-url}")
    private String AUTH_SERVER_URL;

    @Value("${spring.keycloak-users-url}")
    private String USERS_URL;

    @Override
    public Voidy handle(CreateUserCommand command) {
        var checkUser = clientAccessRestTemplate
                .getForObject(UriComponentsBuilder
                        .fromUri(URI.create(AUTH_SERVER_URL + USERS_URL))
                        .queryParam("username", command.getUsername())
                        .build().toUri(), List.class);
        if (!checkUser.isEmpty())
            throw new UserAlreadyExistsException();
        else {
            var headersForCreateUser = new HttpHeaders();
            headersForCreateUser.setContentType(MediaType.APPLICATION_JSON);

            var credentials = new JSONObject();
            var credentialsArray = new JSONArray();
            credentials.put("type", "password");
            credentials.put("temporary", "false");
            credentials.put("value", command.getPassword());
            credentialsArray.put(credentials);

            var request = new JSONObject();
            request.put("credentials", credentialsArray);
            request.put("email", command.getEmail());
            request.put("username", command.getUsername());
            request.put("emailVerified", true);
            request.put("enabled", command.getEnabled());

            HttpEntity<String> entity = new HttpEntity<>(request.toString(), headersForCreateUser);
            log.info(
                    clientAccessRestTemplate.postForObject(AUTH_SERVER_URL + USERS_URL, entity, String.class));

        }
        return new Voidy();
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }
}
