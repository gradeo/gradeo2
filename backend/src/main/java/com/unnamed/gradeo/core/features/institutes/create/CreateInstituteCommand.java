package com.unnamed.gradeo.core.features.institutes.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateInstituteCommand implements Command<Voidy> {
    @Size(min = 2, message = "Введите название института")
    private String name;

}