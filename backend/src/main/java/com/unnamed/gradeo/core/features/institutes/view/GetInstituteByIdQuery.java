package com.unnamed.gradeo.core.features.institutes.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.institutes.InstituteViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetInstituteByIdQuery implements Command<InstituteViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
