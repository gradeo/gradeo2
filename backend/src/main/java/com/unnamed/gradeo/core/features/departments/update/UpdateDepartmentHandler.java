package com.unnamed.gradeo.core.features.departments.update;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchInstituteException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import com.unnamed.gradeo.repositories.InstituteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UpdateDepartmentHandler implements Command.Handler<UpdateDepartmentCommand, Voidy> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final InstituteRepository instituteRepository;
    private final DepartmentsRepository repository;

    public UpdateDepartmentHandler(InstituteRepository instituteRepository, DepartmentsRepository repository) {
        this.instituteRepository = instituteRepository;
        this.repository = repository;
    }

    @Override
    public Voidy handle(UpdateDepartmentCommand command) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchDepartmentException::new);
        logger.debug(LogStrings.FIND_BY_ID, "Institute", command.getInstituteId());
        var institute = instituteRepository
                .findByIdAndRemovedDateIsNull(command.getInstituteId())
                .orElseThrow(NoSuchInstituteException::new);
        logger.debug("Setting name");
        entity.setName(command.getName());
        logger.debug("Setting institute");
        entity.setInstitute(institute);
        logger.debug("Setting updatedDate");
        entity.setUpdatedDate(LocalDateTime.now());

        logger.info(LogStrings.SAVE, entity.getId(), entity.getName());
        repository.save(entity);
        return new Voidy();
    }
}
