package com.unnamed.gradeo.core.features.departments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import com.unnamed.gradeo.core.exceptions.entities.NoSuchDepartmentException;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
//возвращает кафедру по id
public class GetDepartmentByIdHandler implements Command.Handler<GetDepartmentByIdQuery, DepartmentViewDto> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DepartmentsRepository repository;
    private final ModelMapper mapper;

    public GetDepartmentByIdHandler(@NotNull DepartmentsRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public DepartmentViewDto handle(GetDepartmentByIdQuery command) {
        logger.debug(LogStrings.FIND_BY_ID, "Department", command.getId());
        var entity = repository
                .findByIdAndRemovedDateIsNull(command.getId())
                .orElseThrow(NoSuchDepartmentException::new);

        return mapper.map(entity, DepartmentViewDto.class);
    }
}
