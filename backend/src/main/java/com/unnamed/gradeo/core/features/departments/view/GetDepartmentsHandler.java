package com.unnamed.gradeo.core.features.departments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.departments.DepartmentViewDto;
import com.unnamed.gradeo.core.entities.Department;
import com.unnamed.gradeo.repositories.DepartmentsRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
//для возврата всех кафедр
public class GetDepartmentsHandler implements Command.Handler<GetDepartmentsQuery, List<DepartmentViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final DepartmentsRepository repository;

    public GetDepartmentsHandler(ModelMapper mapper, @NotNull DepartmentsRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }


    @Override
    public List<DepartmentViewDto> handle(GetDepartmentsQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Departments");
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();

        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, DepartmentViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.typeMap(Department.class, DepartmentViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getInstitute().getName(), DepartmentViewDto::setInstituteName));
    }

}
