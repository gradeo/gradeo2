package com.unnamed.gradeo.core.features.assessments.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.configuration.LogStrings;
import com.unnamed.gradeo.core.dtos.assessments.AssessmentViewDto;
import com.unnamed.gradeo.core.entities.Assessment;
import com.unnamed.gradeo.repositories.AssessmentsRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetAssessmentsHandler implements Command.Handler<GetAssessmentsQuery, List<AssessmentViewDto>> {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final ModelMapper mapper;
    private final AssessmentsRepository repository;

    @Override
    public List<AssessmentViewDto> handle(GetAssessmentsQuery command) {
        logger.debug(LogStrings.GET_ALL_BY_REMOVED_DATE_IS_NULL, "Assesments");
        var entities = repository.findAllByRemovedDateIsNullOrderByIdAsc();

        return StreamSupport.stream(entities.spliterator(), false)
                .map(entity -> mapper.map(entity, AssessmentViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    void configureMappings() {
        this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.mapper.typeMap(Assessment.class, AssessmentViewDto.class)
                .addMappings(mapper -> mapper.map(d -> d.getTask().getSubject().getId(), AssessmentViewDto::setSubjectId))
                .addMappings(mapper -> mapper.map(d -> d.getTask().getSubject().getName(), AssessmentViewDto::setSubjectName))
                .addMappings(mapper -> mapper.map(d -> d.getTask().getId(), AssessmentViewDto::setTaskId))
                .addMappings(mapper -> mapper.map(d -> d.getTask().getName(), AssessmentViewDto::setTaskName))
                .addMappings(mapper -> mapper.map(d -> d.getTask().getType(), AssessmentViewDto::setTaskType))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getId(), AssessmentViewDto::setStudentId))
                .addMappings(mapper -> mapper.map(d -> d.getStudent().getName(), AssessmentViewDto::setStudentName))
                .addMappings(mapper -> mapper.map(Assessment::getScore, AssessmentViewDto::setScore))
                .addMappings(mapper -> mapper.map(Assessment::getId, AssessmentViewDto::setId));
    }

}
