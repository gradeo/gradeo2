package com.unnamed.gradeo.core.features.assessments.create;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Voidy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateAssessmentCommand implements Command<Voidy> {
    @Positive
    @NotNull(message = "Выберите задание")
    private Integer taskId;

    @Positive
    @NotNull(message = "Выберите студента")
    private Integer studentId;

    @NotNull(message = "Введите оценку за задание")
    @Min(value = 0, message = "Оценка не может быть меньше 0 баллов!")
    @Max(value = 100, message = "Оценка не может быть больше 100 баллов!")
    private Integer score;
}
