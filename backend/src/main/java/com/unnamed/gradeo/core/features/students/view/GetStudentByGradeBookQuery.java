package com.unnamed.gradeo.core.features.students.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.students.StudentViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class GetStudentByGradeBookQuery implements Command<StudentViewDto> {

    @NotNull(message = "Введите номер зачетной книжки студента")
    private Integer gradeBook;
}
