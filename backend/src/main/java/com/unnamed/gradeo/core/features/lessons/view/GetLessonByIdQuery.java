package com.unnamed.gradeo.core.features.lessons.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.lessons.LessonViewDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
public class GetLessonByIdQuery implements Command<LessonViewDto> {

    @NotNull(message = "Не выбран id сущности")
    @Positive
    private final Integer id;
}
