package com.unnamed.gradeo.core.features.semesters.view;

import an.awesome.pipelinr.Command;
import com.unnamed.gradeo.core.dtos.semesters.SemesterViewDto;
import com.unnamed.gradeo.core.entities.Semester;
import com.unnamed.gradeo.repositories.SemestersRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class GetAllSemestersHandler implements Command.Handler<GetAllSemestersQuery, List<SemesterViewDto>> {

    private final SemestersRepository repository;
    private final ModelMapper mapper;

    @Override
    public List<SemesterViewDto> handle(GetAllSemestersQuery command) {
        var semesters = repository.findAllByRemovedDateIsNull();
        return StreamSupport.stream(semesters.spliterator(), false)
                .map(semester -> mapper.map(semester, SemesterViewDto.class))
                .collect(Collectors.toList());
    }

    @PostConstruct
    private void configureMappings() {
        this.mapper.typeMap(Semester.class, SemesterViewDto.class)
                .addMappings(mapping -> mapping.map(Semester::getStudentsNames, SemesterViewDto::setStudentsNames))
                .addMappings(mapping -> mapping.map(Semester::getSubjectsNames, SemesterViewDto::setSubjectsNames))
                .addMappings(mapping -> mapping.map(source -> source.getGroup().getId(), SemesterViewDto::setGroupId));
    }
}
