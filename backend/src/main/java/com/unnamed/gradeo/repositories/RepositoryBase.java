package com.unnamed.gradeo.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface RepositoryBase<TType, TKey extends Serializable> extends CrudRepository<TType, TKey>, JpaSpecificationExecutor<TType> {
    Iterable<TType> findAllByRemovedDateIsNullOrderByIdAsc();

}
