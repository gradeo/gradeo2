package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Subject;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectsRepository extends RepositoryBase<Subject, Integer> {
    @Override
    Optional<Subject> findById(Integer integer);

    @EntityGraph(attributePaths = "department")
    Optional<Subject> findByIdAndRemovedDateIsNull(Integer id);

    Iterable<Subject> findAllByDepartmentIdAndRemovedDateIsNull(Integer integer);

    Iterable<Subject> findAllByTeachersIdAndRemovedDateIsNull(Integer integer);

    List<Subject> findAllBySemestersIdAndRemovedDateIsNull(Integer id);

    List<Subject> findAllByNameAndRemovedDateIsNull(String name);
}
