package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupsRepository extends RepositoryBase<Group, Integer> {
    @Override
    Optional<Group> findById(Integer id);

    Optional<Group> findByIdAndRemovedDateIsNull(Integer id);

    @EntityGraph(attributePaths = "department")
    Optional<Group> findByNameAndRemovedDateIsNull(String name);

    Iterable<Group> findAllByDepartmentIdAndRemovedDateIsNull(Integer integer);

    List<Group> findAllBySemestersIdAndRemovedDateIsNull(Integer id);

    Iterable<Group> findAllByNameAndRemovedDateIsNull(String name);


}
