package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.User;

import java.util.Optional;


public interface UserRepository extends RepositoryBase<User, Integer> {
    User findByUsernameAndRemovedDateIsNull(String username);

    User findByEmailAndRemovedDateIsNull(String email);

    User findByUsername(String username);

    Optional<User> findById(Integer integer);
}
