package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Attendance;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Репозиторий посещаемости.
 */
@Repository
public interface AttendanceRepository
        extends RepositoryBase<Attendance, Integer>,
        JpaSpecificationExecutor<Attendance> {

    Optional<Attendance> findByIdAndRemovedDateIsNull(Integer integer);

    Iterable<Attendance> findAllByLessonIdAndRemovedDateIsNull(Integer integer);

    Optional<Attendance> findByStudentId(Integer integer);

}

