package com.unnamed.gradeo.repositories;

import com.unnamed.gradeo.core.entities.Teacher;
import org.springframework.data.jpa.repository.EntityGraph;

import java.util.Optional;

public interface TeachersRepository extends RepositoryBase<Teacher, Integer> {
    @Override
    Optional<Teacher> findById(Integer integer);

    Optional<Teacher> findByIdAndRemovedDateIsNull(Integer id);

    Iterable<Teacher> findAllByDepartmentIdAndRemovedDateIsNull(Integer id);

    Iterable<Teacher> findAllByNameAndRemovedDateIsNull(String name);

    Iterable<Teacher> findAllBySubjectsIdAndRemovedDateIsNull(Integer id);

    Optional<Teacher> findByUserId(Integer id);
}
